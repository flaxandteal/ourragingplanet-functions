import random
from orp_sim_firstgen import utils
from orp_sim_firstgen.risk import Risk
from orp_sim_firstgen.result_service import ResultService
from flask import current_app

class SurgeRisk(Risk): # This class is used for the risk of a flood and contains its vars
    _risk_range = (0.0, 500.0) # Range of risk

    _flood_start = 0 # Time for flood start
    _flood_rate = 20000.0  # mm / hr
    _flood_duration = 10 * 3600 # s
    _flood_initial_level = 1200 # m RMV

    @classmethod
    def preload(cls):
        return {'heights'}

    def advance_time(self, time): # This function advances the time
        height = max(0, self._current_flood_height(time)) # Setting the max value of the flood height, 0 and the time of the current flood height
        self._logger.info("Height: %f" % height) # Place height into log
        self._height = height # Set height value

    def _current_flood_height(self, current_time):  # This function determines the current flood height
        if current_time - self._flood_start > self._flood_duration: # If the current time minus the flood start is more than the flood duration...
            current_time = self._flood_start + self._flood_duration # Current time becomes flood start plus the flood duration

        return self._flood_initial_level + (current_time - self._flood_start) * (self._flood_rate / (3600 * 1000))

    def load(self, logger, center, z, **parameters): # Loading function
        logger.info("Starting height: %f" % z) # place starting height into the log
        self._logger = logger # set up logger object for this class
        #self._flood_initial_level = z # becomes the inital level for the flood
        self._center = center

    # Are x and y definitely in SI?
    def calculate(self, x, z, i, j):
        variation = random.random() * 0.2 - 0.1
        if z > self._height + self._risk_range[1] + variation:  # if z is more than the height plus the second value in risk_range...
            return 0.0
        elif z < self._height + self._risk_range[0] + variation: # else if z is less than the height plus the first value in risk_range...
            return 1.0
        else: # if all other conditions fail go here...
            return (self._height - z + self._risk_range[1]) / (self._risk_range[1] - self._risk_range[0]) + variation #As waves get higher risk gets higher

    def finish(self):
        pass

def risk():
    return SurgeRisk()
