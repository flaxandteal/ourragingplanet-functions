from orp_sim_firstgen import utils
from orp_sim_firstgen.risk import Risk
from orp_sim_firstgen.result_service import ResultService
from flask import current_app
from math import sin, cos
from scipy.stats import skewnorm

class SandstormSimpleDriftRisk(Risk): # limited model showing of distance w concentric damage
    _risk_range = (500, 5000)   # Setting up range of a particular risk
    _time = 0   # Setting up time for a particular risk
    _start_time = None  #  Start time for a particular risk - none in this case
    _alpha = 4.

    def __init__(self):
        # gives us a more rectilinear frame of ref
        self._converter = utils.IrishGridConverter()
        self._epicenter = []

    @classmethod
    def preload(cls):
        return set()

    def advance_time(self, time):  # Function for advancing the time
        self._time = time # The time that's passed in through the parameter for this function is assigned to self

    def load(self, logger, center, z, **parameters):  # Load function: loads risk range,logger,epicentre,and parameters
        logger.info("Starting risk range: %f -> %f" % self._risk_range)
        self._logger = logger
        c = self._converter(*center, inverse=False)

        self._parameters = parameters
        if 'speed' in parameters:
            speed = float(parameters['speed'])
        else:
            speed = 0

        if 'direction' in parameters:
            direction = float(parameters['direction'])
        else:
            direction = 0

        logger.info("Set up drifting centre with speed %f and direction (rad fm N) %f", speed, direction)
        pairs = [
            (0, 1.0),
            (4500, 1.0),
            (9000, 1.0),
            (13500, 1.0),
            (18000, 1.0),
            (23500, 1.0)
        ]
        for t, r in pairs:
            d = [c[0] + t * speed * cos(direction) * 5e2, c[1] + t * speed * sin(direction) * 5e2]
            self._epicenter.append((t, d, r))
        self._direction = direction

    # Are x and y definitely in SI?
    def calculate(self, x, z, i, j): # Calcuating the risk
        x = self._converter(*x, inverse=False) # X is a vector,
        risk = 0.0 # Starting value for risk

        for time, center, scaling in self._epicenter: # For loop for epicentre
            if time > self._time: # If this epicentre slice time is less than the time declared in self continue
                continue

            # Do we want quadratic scaling?

            cap = 1.0

            direction_dot = ((x[0] - center[0]) * cos(self._direction) + (x[1] - center[1]) * sin(self._direction))
            direction_norm = ((x[0] - center[0]) * sin(self._direction) - (x[1] - center[1]) * cos(self._direction))
            distance_squared = direction_dot ** 2 / self._alpha + direction_norm ** 2
            risk = min(cap, max(0.0, scaling * (1 - (distance_squared - self._risk_range[0] ** 2) / (self._risk_range[1] ** 2 - self._risk_range[0] ** 2))))
            if direction_dot > 0:
                risk = 0

        return risk

    def finish(self): #  Finish function
        pass

def risk():
    return SandstormSimpleDriftRisk()
