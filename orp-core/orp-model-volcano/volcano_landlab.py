# Based on
# overland_flow_with_model_drid_dem.py
# Landlab MIT-licensed

from math import sqrt, log, exp, log10
from scipy.special import erf
from pyproj import Geod
import landlab
from landlab.io import read_esri_ascii
import time
import os
import pylab
import numpy as np

from landlab.plot import imshow_grid
from landlab import FIXED_VALUE_BOUNDARY, RasterModelGrid
from landlab.grid.divergence import calc_flux_div_at_node

from orp_sim_firstgen import utils
from orp_sim_firstgen.risk import Risk

class VolcanoLavaRisk(Risk):
    _risk_range = (50, 500) #Range of risk
    _risk_time_ratio = 0.001 #Time ratio of risk
    _time = 0 # Init time

    _alpha = 0.2 # time-step factor (ND; from Bates et al., 2010)
    _mg = None
    _ele = None
    _height = None
    _g = 9.81 # gravitational acceleration (m/s2)
    _h_init = 0.001        # initial thin layer of water (m)
    _roughness = 0.03              # roughness coefficient (Manning's n)
    _rainfall_rate = 0.00001 #(rainfall_mmhr/1000.)/3600.  # rainfall in m/s
    _outlet_row = 6
    _outlet_column = 38
    _next_to_outlet_row = 7
    _next_to_outlet_column = 38

    _core_nodes = None
    _outlet_node = None
    _node_next_to_outlet = None

    def __init__(self):
        # gives us a more rectilinear frame of ref
        self._converter = utils.IrishGridConverter()
        self._epicenter = []

    @classmethod
    def preload(cls):
        return set()

    def advance_time(self, time):
        current_time = self._time * 0.001
        grid = self._mg
        ele = self._ele
        height = self._height
        gravity = self._g
        discharge = self._discharge
        roughness = self._roughness

        while current_time < time * 0.001:
            self._logger.info("Advancing to %f - up to %lf" % (current_time, time))
            # Calculate time-step size for this iteration (Bates et al., eq 14)
            dtmax = self._alpha*grid.dx/np.sqrt(gravity * np.amax(height))
            self._logger.info(np.amax(height))
            self._logger.info(np.amax(dtmax))

            # Calculate the effective flow depth at active links. Bates et al. 2010
            # recommend using the difference between the highest water-surface
            # and the highest bed elevation between each pair of cells.
            zmax = grid.max_of_link_end_node_values(ele)
            w = height + ele   # water-surface height
            wmax = grid.max_of_link_end_node_values(w)
            hflow = wmax - zmax

            # Calculate water-surface slopes
            #water_surface_slope = mg.calculate_gradients_at_active_links(w)
            water_surface_slope = grid.calc_grad_at_link(w)[grid.active_links]

            # Calculate the unit discharges (Bates et al., eq 11)
            ten_thirds = 10. / 3.
            self._logger.info(np.amax(hflow))
            self._logger.info(np.amax(1. + gravity * hflow * dtmax * roughness * roughness * abs(discharge)))
            self._logger.info(np.amin(hflow))
            self._logger.info(np.amin(1. + gravity * hflow * dtmax * roughness * roughness * abs(discharge)))
            hflow[np.abs(hflow) < 10e-13] = 10e-13
            discharge = (discharge - gravity * hflow * dtmax * water_surface_slope) / \
                (1. + gravity * hflow * dtmax * roughness * roughness * abs(discharge) / (hflow ** ten_thirds))

            # Calculate water-flux divergence at nodes
            dqds = calc_flux_div_at_node(grid, discharge)

            # Calculate rate of change of water depth
            dhdt = self._rainfall_rate - dqds

            # Second time-step limiter (experimental): make sure you don't allow
            # water-depth to go negative
            if np.amin(dhdt) < 0.:
                shallowing_locations = np.where(dhdt<0.)
                time_to_drain = -height[shallowing_locations]/dhdt[shallowing_locations]
                dtmax2 = self._alpha * np.amin(time_to_drain)
                dt = np.min([dtmax, dtmax2])
                self._logger.info('dtmax2')
                self._logger.info(dtmax2)
            else:
                dt = dtmax
            self._logger.info(dt)

            # Update the water-depth field
            core_nodes = self._core_nodes
            height[core_nodes] = height[core_nodes] + dhdt[core_nodes] * dt
            height[self._outlet_node] = height[self._node_next_to_outlet]
            yl, xl = self._mg.shape
            cx = xl // 2
            cy = yl // 2
            for i in range(cy - 5, cy + 5):
                height[cx - 5 + i * xl:cx + 5 + i * xl] = 10

            # Update current time
            current_time += dt

            self._discharge = discharge

        self._time = time

    def load(self, logger, center, z, heights, **parameters):
        logger.info("Starting risk range: %f -> %f" % self._risk_range)
        self._logger = logger
        self._epicenter = self._converter(*center, inverse=False)
        self._parameters = parameters

        # Create and initialize a raster model grid by reading a DEM
        #(mg, z) = read_esri_ascii(DATA_FILE)
        xx, yy, ele = heights
        xc = np.array(sorted(set(xx.flatten())))
        yc = np.array(sorted(set(yy.flatten())))
        logger.info(xc)

        mg = RasterModelGrid((len(yc), len(xc)), spacing=(50, 50))
        mg.set_closed_boundaries_at_grid_edges(False, False, False, True)
        self._outlet_node = mg.grid_coords_to_node_id(self._outlet_row, self._outlet_column)
        self._node_next_to_outlet = mg.grid_coords_to_node_id(
            self._next_to_outlet_row,
            self._next_to_outlet_column
        )
        mg.status_at_node[self._outlet_node] = FIXED_VALUE_BOUNDARY
        mg.update_boundary_nodes()

        # Set up state variables
        height = mg.add_zeros('node', 'Water_depth') + self._h_init     # water depth (m)
        #leftside = mg.nodes_at_left_edge
        #self._logger.info(leftside)
        self._logger.info("_____")
        #uppercorner = len(leftside) // 10
        yl, xl = mg.shape
        cx = xl // 2
        cy = yl // 2
        for i in range(cy - 5, cy + 5):
            height[cx - 5 + i * xl:cx + 5 + i * xl] = 10
        logger.info(np.amax(height))

        #discharge = mg.create_active_link_array_zeros()       # unit discharge (m2/s)
        self._discharge = np.zeros(mg.number_of_active_links)

        # Get a list of the core nodes
        self._core_nodes = mg.core_nodes

        self._mg = mg
        self._ele = ele
        self._height = height

        logger.info("Loaded volcano lava model")

    # Are x and y definitely in SI?
    def calculate(self, x, z, i, j):
        # x = self._converter(*x, inverse=False)

        shape = self._mg.shape
        risk = self._height[i + shape[1] * j]
        if np.isnan(risk):
            risk = 1.0

        return risk

    def finish(self):
        pass
