from orp_sim_firstgen import utils
from orp_sim_firstgen.risk import Risk
from orp_sim_firstgen.result_service import ResultService
from flask import current_app

class ConcentricRisk(Risk): # limited model showing of distance w concentric damage
    _risk_range = (500, 5000)   # Setting up range of a particular risk
    _time = 0   # Setting up time for a particular risk
    _start_time = None  #  Start time for a particular risk - none in this case

    def __init__(self):
        # gives us a more rectilinear frame of ref
        self._converter = utils.IrishGridConverter()
        self._epicenter = []

    @classmethod
    def preload(cls):
        return set()

    def advance_time(self, time):  # Function for advancing the time
        self._time = time # The time that's passed in through the parameter for this function is assigned to self

    def load(self, logger, center, z, **parameters):  # Load function: loads risk range,logger,epicentre,and parameters
        logger.info("Starting risk range: %f -> %f" % self._risk_range)
        self._logger = logger
        c = self._converter(*center, inverse=False)
        self._epicenter.append((0, c, 0.0))
        self._epicenter.append((4500, c, 0.3))
        self._epicenter.append((9000, c, 0.8))
        self._epicenter.append((18000, c, 1.0))
        self._parameters = parameters

    # Are x and y definitely in SI?
    def calculate(self, x, z, i, j): # Calcuating the risk
        x = self._converter(*x, inverse=False) # X is a vector,
        risk = 0.0 # Starting value for risk

        for time, center, scaling in self._epicenter: # For loop for epicentre
            if time > self._time: # If this epicentre slice time is less than the time declared in self continue
                continue

            distance_squared = (x[0] - center[0]) ** 2 + (x[1] - center[1]) ** 2
            # Do we want quadratic scaling?

            cap = 1.0

            risk = min(cap, max(0.0, scaling * (1 - (distance_squared - self._risk_range[0] ** 2) / (self._risk_range[1] ** 2 - self._risk_range[0] ** 2))))

        return risk

    def finish(self): #  Finish function
        pass

def risk():
    return ConcentricRisk()
