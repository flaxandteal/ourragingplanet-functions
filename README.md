# OurRagingPlanet (ORP) Simulation Functions

## Flax &amp; Teal Limited

This tool is AGPL - this means you cannot provide functionality using this code, or portions of it, over a network, without providing the source code to your own modifications. Please note, that the entire stack will be, for these purposes, considered a combined work -- if you are not willing to open source your interface, infrastructure and all tools involved in serving this to end users (live or batched), you MUST NOT use this tool without another agreed license. Please note, that this does not impact manually running the tool or serving it only for yourself (whether using the outputs commercially or non-commercially).

Use of this tool, or component code, to provide services indicates acceptance of these terms.

Merge requests are accepted as MIT-licensed contributions only, even though the combined codebase is AGPL - for more information, please see https://gitlab.com/openaugury/openaugury-backend/ .

For relicensing options other than AGPL (either commercial or non-commercial) or rights to use the OurRagingPlanet brand, please contact info@flaxandteal.co.uk
