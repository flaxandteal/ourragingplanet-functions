We gratefully acknowledge the work done by a number of third-parties, especially for providing these components we have reused as open source.

These licenses apply to specific third-party code.
They do not apply to this combined work, and do not set aside any requirements of AGPL license applied to this codebase.
Please use the original third-party versions, if you wish to use the original licenses.
